#!/usr/bin/env python
# -*- coding: utf-8 -*-

__version__ = u'0.2'
__author__ = u'Silvjor'

from app import app

from views import *

from routing import *
from gps import *

from scheduler import scheduler

if __name__ == '__main__':
    # start the instance
    #scheduler.start()

    app.run(use_reloader=False)
