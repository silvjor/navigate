#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
views imports app, auth, and models, but none of these import views
"""

import os

from flask import render_template, send_from_directory

from app import app


# Index
@app.route('/')
def index():
    return send_from_directory('/home/silvjor/Coding/navigate/GUI/','index.htm')

@app.route('/<path:path>')
def send_js(path):
    return send_from_directory('/home/silvjor/Coding/navigate/GUI/', path)


@app.errorhandler(404)
def page_not_found(e):
    """Return a custom 404 error."""
    return 'Sorry, Nothing at this URL.', 404


@app.errorhandler(500)
def page_not_found(e):
    """Return a custom 500 error."""
    return 'Sorry, unexpected error: {}'.format(e), 500
