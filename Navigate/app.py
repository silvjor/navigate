#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import Flask

app = Flask(__name__)

app.config.update(
    DEBUG=True,
    SECRET_KEY='NiFXYxxwZ8alpL0ARGtk6v8JnwrLQoqd',
    SERVER_NAME='127.0.0.1:8989'
)
