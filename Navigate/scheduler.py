#!/usr/bin/env python
# -*- coding: utf-8 -*-

#must be last imported file

from app import app

from flask_apscheduler import APScheduler


# create flask_apscheduler instance
scheduler = APScheduler()


# assign to app
scheduler.init_app(app)


# start the instance
scheduler.start()
