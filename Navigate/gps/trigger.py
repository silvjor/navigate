#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import time
import datetime
import csv


from USB_GPS import GPS

from gps.data import *
from gps import TESTING


from app import app


if TESTING:
    print("TEST DATA")

    GPS_FILES = [[ f.split('_')[0], f] for f in os.listdir( os.path.dirname(__file__) ) if os.path.isfile(os.path.join( os.path.dirname(__file__) , f)) if '_gps' in f]

    DATA_FILE = os.path.join(os.path.dirname(__file__),  [f for f in sorted(GPS_FILES, key = lambda x: int(x[0]))][0][1] )

    GPS_POSITIONS = []
    START_TIME_LOG = False


    with open(DATA_FILE, 'r', newline='') as csvfile:
        csvread = csv.reader(csvfile, delimiter=',', quotechar='|')

        # skip first line
        next(csvread)

        for row in csvread:

            if not START_TIME_LOG:
                START_TIME_LOG = int( time.mktime( datetime.datetime.strptime(row[0].split('.')[0], '%Y-%m-%d %H:%M:%S').timetuple() ) )

            GPS_POSITIONS.append([
                int( time.mktime( datetime.datetime.strptime(row[0].split('.')[0], '%Y-%m-%d %H:%M:%S').timetuple() ) ) - START_TIME_LOG,
                *row[1:]
            ])

    START_TIME_CURRENT = int( time.mktime( datetime.datetime.now().timetuple() ) )

    #print(START_TIME_CURRENT-START_TIME_LOG)

    def update():

        CURRENT_TIME_N = int( time.mktime( datetime.datetime.now().timetuple() ) ) - START_TIME_CURRENT

        for row in GPS_POSITIONS:
            if row[0] == CURRENT_TIME_N:
                app._gps_._insert_end( time.mktime(time.localtime( row[0] + START_TIME_CURRENT ) ), row[1], row[2], row[4] )
                break

        app._gps_.GPS_STATE = True
        app._gps_.GPS_FIX = True





else:

    GPS = GPS()

    GPS.update()


    def update():

        GPS.update()

        if GPS.connected:
            app._gps_._insert_end( time.mktime(GPS.rmc['timestamp'].timetuple()), GPS.rmc['latitude'], GPS.rmc['longitude'], GPS.gsa['pdop'] )

        app._gps_.GPS_STATE = GPS.connected
        app._gps_.GPS_FIX = GPS.fix
