#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import datetime

import numpy as np

from gps.speed import speed, distance, calculate_initial_compass_bearing

from app import app


__all__=['GPS_DATA']


class GPS_DATA(object):

    ARRAY_SIZE = 80

    # order: time, lat, lon, dop
    NP_ARRAY = np.zeros((ARRAY_SIZE,4), dtype=float)

    GPS_FIX = False
    GPS_STATE = False


    def _get_nonzero(self):

        # create list of nonzero values and return the array
        return self.NP_ARRAY[ np.unique( np.nonzero(self.NP_ARRAY)[0] ) ]


    def _insert_end(self, time, lat, lon, dop):

        # push values at end
        self.NP_ARRAY[:-1] = self.NP_ARRAY[1:]; self.NP_ARRAY[-1] = [time, lat, lon, dop]


    def dataPresent(self):
        if sum( self.NP_ARRAY[-1] ) != 0:
            return True
        return False


    def current(self):

        # return None if no data present
        if not self.dataPresent():
            return {'latitude': None, 'longitude': None, 'time': None, 'precission': None}

        # return latest entry as dict
        return {'latitude': self.NP_ARRAY[-1][1], 'longitude': self.NP_ARRAY[-1][2], 'time': self.NP_ARRAY[-1][0], 'precission': self.NP_ARRAY[-1][3]}


    def previous(self, n=0):

        n += 2

        # return None if no data present
        if sum( self.NP_ARRAY[-n] ) != 0:
            return {'latitude': None, 'longitude': None, 'time': None, 'precission': None}

        # return latest entry as dict
        return {'latitude': self.NP_ARRAY[-n][1], 'longitude': self.NP_ARRAY[-n][2], 'time': self.NP_ARRAY[-n][0], 'precission': self.NP_ARRAY[-n][3]}


    def speed(self, points=2):
        points = points if points >= 2 else 2
        kmh = 0

        for i in range(1,points):
            if sum( self.NP_ARRAY[-i] ) != 0 and sum( self.NP_ARRAY[-i-1] ) != 0:
                kmh += speed(
                    {'latitude': self.NP_ARRAY[-i][1], 'longitude': self.NP_ARRAY[-i][2], 'time': self.NP_ARRAY[-i][0], 'precission': self.NP_ARRAY[-i][3]},
                    {'latitude': self.NP_ARRAY[-i-1][1], 'longitude': self.NP_ARRAY[-i-1][2], 'time': self.NP_ARRAY[-i-1][0], 'precission': self.NP_ARRAY[-i-1][3]}
                )

        return kmh / (points-1)

    PREV_HEADING = 0

    def heading(self, points=5):
        points = points if points >= 2 else 2

        # test if more then one is present
        if not all( [True if sum(i) != 0 else False for i in self.NP_ARRAY[-2:] ] ):
            return 0

        if self.speed() < 3:
            return self.PREV_HEADING

        heading = 0

        for i in range(1,points):
            heading += calculate_initial_compass_bearing(tuple(self.NP_ARRAY[-i][1:3]),tuple(self.NP_ARRAY[-i-1][1:3]))

        self.PREV_HEADING = ( heading / (points-1) ) + 180

        return self.PREV_HEADING


    def derp(self):

        print( sum( self.NP_ARRAY[-1] ) != 0 )

        self._insert_end(1,2,3,4)

        print( sum( self.NP_ARRAY[-1] ) != 0 )

        return None


app._gps_ = GPS_DATA()
