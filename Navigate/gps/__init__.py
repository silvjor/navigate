#!/usr/bin/env python
# -*- coding: utf-8 -*-

__version__ = u'0.1'
__author__ = u'Silvjor'

# boolean for testing switch
TESTING = True

from app import app

from gps.data import *
from gps.trigger import update


# scheduler settings
class GPS_Scheduler(object):
    JOBS = [
        {
            'id': 'updateGPS',
            'func': 'gps:update',
            'trigger': 'interval',
            'seconds': 1
        }
    ]

    SCHEDULER_API_ENABLED = True


# add gps interval to app
app.config.from_object( GPS_Scheduler() )
