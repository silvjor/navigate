#!/usr/bin/env python
# -*- coding: utf-8 -*-

import math
import time

__all__=['speed','distance']

def distance(origin, destination):
    lat1, lon1 = origin
    lat2, lon2 = destination
    radius = 6371 # km

    dlat = math.radians(lat2-lat1)
    dlon = math.radians(lon2-lon1)
    a = math.sin(dlat/2) * math.sin(dlat/2) + math.cos(math.radians(lat1)) \
        * math.cos(math.radians(lat2)) * math.sin(dlon/2) * math.sin(dlon/2)
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
    d = radius * c

    return d


def speed(current, previous):

    if current['latitude'] == None or previous['latitude'] == None:
        return 0.0

    origin = [ current['latitude'], current['longitude']]
    destination = [ previous['latitude'], previous['longitude']]

    #timeA = time.mktime(current['time'].timetuple())
    #timeB = time.mktime(previous['time'].timetuple())

    timeA = current['time']
    timeB = previous['time']

    dist = distance(origin, destination)

    time_s = (timeA - timeB) / 1000.0
    speed_mps = dist / time_s
    speed_kph = (speed_mps * 3600.0) / 1000.0

    return speed_kph

#https://gist.github.com/jeromer/2005586
def calculate_initial_compass_bearing(pointA, pointB):
    """
    Calculates the bearing between two points.
    The formulae used is the following:
        θ = atan2(sin(Δlong).cos(lat2),
                  cos(lat1).sin(lat2) − sin(lat1).cos(lat2).cos(Δlong))
    :Parameters:
      - `pointA: The tuple representing the latitude/longitude for the
        first point. Latitude and longitude must be in decimal degrees
      - `pointB: The tuple representing the latitude/longitude for the
        second point. Latitude and longitude must be in decimal degrees
    :Returns:
      The bearing in degrees
    :Returns Type:
      float
    """
    if (type(pointA) != tuple) or (type(pointB) != tuple):
        raise TypeError("Only tuples are supported as arguments")

    lat1 = math.radians(pointA[0])
    lat2 = math.radians(pointB[0])

    diffLong = math.radians(pointB[1] - pointA[1])

    x = math.sin(diffLong) * math.cos(lat2)
    y = math.cos(lat1) * math.sin(lat2) - (math.sin(lat1)
            * math.cos(lat2) * math.cos(diffLong))

    initial_bearing = math.atan2(x, y)

    # Now we have the initial bearing but math.atan2 return values
    # from -180° to + 180° which is not what we want for a compass bearing
    # The solution is to normalize the initial bearing as shown below
    initial_bearing = math.degrees(initial_bearing)
    compass_bearing = (initial_bearing + 360) % 360

    return compass_bearing


def bearing(latA, lonA, latB, lonB):
		deltaLon = math.radians(lonB) - math.radians(lonB)
		deltaLat = math.radians(latA) - math.radians(latB)
		lat1 = math.radians(latB)
		lat2 = math.radians(latA)
		# http://gis.stackexchange.com/questions/29239/calculate-bearing-between-two-decimal-gps-coordinates
		dPhi = math.log(math.tan(lat2/2.0+math.pi/4.0)/math.tan(lat1/2.0+math.pi/4.0))
		if abs(deltaLon) > math.pi:
			if deltaLon > 0.0:
				deltaLon = -(2.0 * math.pi - dLong)
			else:
				deltaLon = (2.0 * math.pi + dLong)

		bearing = (math.degrees(math.atan2(deltaLon, dPhi)) + 360.0) % 360.0
		return round(bearing, 6)
