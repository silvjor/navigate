#!/usr/bin/env p    ython
# -*- coding: utf-8 -*-

from flask import request
from flask_restful import Resource

from app import app

class Realtime(Resource):

    def get(self):


        current = app._gps_.current()

        return {
            'position': {
                'latitude': current['latitude'],
                'longitude': current['longitude'],
                'heading': app._gps_.heading()
            }

        }


# get current name by testing on which lane we are
