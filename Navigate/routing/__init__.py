#!/usr/bin/env python
# -*- coding: utf-8 -*-

__version__ = u'0.1'
__author__ = u'Silvjor'

from flask import  request
from flask_restful import Api

from app import app

from gps.data import *
from routing.data import *

from routing.navigate import Navigate
from routing.realtime import Realtime
#from routing.location import Location
#from routing.gps import GPS


api = Api(app)


api.add_resource(Navigate, '/navigate')
api.add_resource(Realtime, '/realtime')
#api.add_resource(Location, '/location')
#api.add_resource(GPS, '/gps')

"""

PUT
    coordinate for navigation
        /navigate

GET
    location by name
        /location
    near location
        /location/{current,city,coordinate}

    current route as geojson
        /route

    turns with distance from them
        /turns

    list of variables: GPS fix, current position, last route update, on route
        /realtime


"""
