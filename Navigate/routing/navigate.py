#!/usr/bin/env p    ython
# -*- coding: utf-8 -*-

import lib.osrm

from flask import request
from flask_restful import Resource

from app import app

class Navigate(Resource):

    def __init__(self):
        self.client = lib.osrm.Client(host='http://127.0.0.1:1111')

    def put(self):

        if not 'type' in request.form:
            return {}

        if request.form['type'] == 'coord':

            # check if lat and lon are set
            if all([elem in request.form for elem in ['lat','lon']]):

                if not app._gps_.dataPresent():
                    return {'GPS_FIX':False}

                # get current gps coord
                current = app._gps_.current()
                current = [current['latitude'], current['longitude']]

                # set target coord
                app.ROUTING.TARGET = [ float(request.form['lat']), float(request.form['lon']) ]


                ROUTE_N = 0
                ROUTE_MULTI = False

                # test if user wants specific route from multiple options
                if 'route' in request.form:
                    if request.form['route'].isdigit():
                        ROUTE_N = int(request.form['route'])
                        ROUTE_MULTI = True


                # request route from OSRM
                response = self.client.route(
                            coordinates=[current[::-1], app.ROUTING.TARGET[::-1]],
                            alternatives=ROUTE_MULTI,
                            overview=lib.osrm.overview.full,
                            annotations=False, # speed and other stuff
                            steps=True,
                            geometries=lib.osrm.geometries.geojson
                        )

                if len(response['routes']) > ROUTE_N:
                    ROUTE_N = 0

                # set route
                app.ROUTING.ROUTE = response['routes'][ROUTE_N]['geometry']['coordinates']

                # reset turns
                app.ROUTING.TURNS = []

                # itterate turns
                for turn in response['routes'][ROUTE_N]['legs'][0]['steps']:

                    index_start = False
                    index_end = False

                    # itterate seperate coords
                    for coord in turn['geometry']['coordinates']:

                        # find in coordinates
                        for index, item in enumerate(app.ROUTING.ROUTE):
                            if item[0] == coord[0] and item[1] == coord[1]:
                                if not index_start:
                                    index_start = index
                                index_end = index
                                break

                    #print(index_start, index_end, turn['name'], turn['maneuver']['modifier'] if 'modifier' in turn['maneuver'] else False)
                    app.ROUTING.TURNS.append({
                        'index': [index_start, index_end],
                        'name': turn['name'],
                        'maneuver': [i for i in turn['maneuver'] if not i in ['bearing_after','bearing_before','location']],
                        'distance': turn['distance']
                    })


                return {'GPS_FIX':True, 'ROUTE': app.ROUTING.ROUTE, 'TARGET': app.ROUTING.TARGET}

            else:
                return {}

        return {}


#curl -X PUT -d type=coord -d lat=53.210511 -d lon=5.780439 http://127.0.0.1:8989/navigate
