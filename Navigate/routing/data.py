#!/usr/bin/env python
# -*- coding: utf-8 -*-


from app import app


__all__=['ROUTING']


# initialize the data into app

class ROUTING(object):

    TARGET = [None, None] # list: lat, lon

    OFF_ROUTE_BOOL = False #bool
    OFF_ROUTE_TIME = 0 #int

    ROUTE = None #dict
    HEADING = 0 #degrees

    TURNS = None #dict


app.ROUTING = ROUTING()
