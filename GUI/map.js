var map;

function easing(t) {
  return t;
}

    // map updating trhough live data
var mapUpdates = {

  update: function(){

    var url = '/realtime';

    var xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        var json = JSON.parse(this.responseText);

        //mapUpdates.drawRoute( json['currentRoute'], json['traversed'] );

        mapUpdates.alignToData( json['position']['longitude'], json['position']['latitude'], json['position']['heading'] );


      }

    };

    xmlhttp.open("GET", url, true);
    xmlhttp.send();

  },

  drawRoute: function(route, traversed){

    /*map.addLayer({
        "id": "route",
        "type": "line",
        "source": {
            "type": "geojson",
            "data": {
                "type": "Feature",
                "properties": {},
                "geometry": {
                    "type": "LineString",
                    "coordinates": traversed
                }
            }
        },
        "layout": {
            "line-join": "round",
            "line-cap": "round"
        },
        "paint": {
            "line-color": "#2d5996",
            "line-width": 3
        }
    });*/

    map.addLayer({
        "id": "route",
        "type": "line",
        "source": {
            "type": "geojson",
            "data": {
                "type": "Feature",
                "properties": {},
                "geometry": {
                    "type": "LineString",
                    "coordinates": route['coordinates']
                }
            }
        },
        "layout": {
            "line-join": "round",
            "line-cap": "round"
        },
        "paint": {
            "line-color": "#4990e2",
            "line-width": 4
        }
    });

  },

  alignToData: function(lat, lon, heading){

    if(AUTO_ALIGN){

      console.log(lat, lon, heading);

      //map.setCenter([lat, lon]);
      //map.setBearing(heading);


      map.easeTo({
        center: [lat, lon],
        bearing: heading,
        easing: easing,
        duration: 2100
     });

     //map.flyTo({center: [lat, lon]});




      /*map.easeTo({
                    bearing: heading,
                    center: [currentLatLon[0], currentLatLon[1]],
                    easing: easing
                });
*/

    }

  }

}




window.onload = function(){

  map = new mapboxgl.Map({
    container: 'map',
    style: MAP_URL,
    hash: true
  });


  map.addControl(new mapboxgl.NavigationControl());

  map.on('load', function () {

    map.setLayoutProperty('omt_watermark', 'visibility', 'none');
    map.setLayoutProperty('water-pattern', 'visibility', 'none');

    MAP_LOADED = true;

    // create map updating interval
    setInterval(function(){
      mapUpdates.update();
    }, 800);

    forceRoute();

  });

}
