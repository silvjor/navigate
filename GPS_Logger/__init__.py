#!/usr/bin/env python
# -*- coding: utf-8 -*-

__version__ = u'0.1'
__author__ = u'Silvjor'


import sys
import os
import time
import sched
import csv

from USBgps import GPS


GPS = GPS()

DATA_FILE = os.path.join(os.path.dirname(__file__), str(int(time.time()))+'_gps.txt')


with open(DATA_FILE, 'w', newline='') as csvfile:
    spamwriter = csv.writer(csvfile, delimiter=',',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
    spamwriter.writerow(['Fix Time', 'Latitude', 'Longitude', '3D Fix', 'Dilution of Precision'])
    csvfile.close()


def RUN():
    GPS.update()

    with open(DATA_FILE, 'a', newline='') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=',',
                                quotechar='|', quoting=csv.QUOTE_MINIMAL)
        spamwriter.writerow([GPS.rmc['timestamp'], GPS.rmc['latitude'], GPS.rmc['longitude'], GPS.gsa['mode'], GPS.gsa['pdop']])
        csvfile.close()


if __name__ == '__main__':

    s = sched.scheduler(time.time, time.sleep)

    def LOOP(sc):
        s.enter(1, 1, LOOP, (sc,))

        RUN()

    s.enter(1, 1, LOOP, (s,))

    try:
        s.run()
    except (KeyboardInterrupt, SystemExit):
        print('Exiting')
        sys.exit()
