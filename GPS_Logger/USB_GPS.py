#!/usr/bin/env python
# -*- coding: utf-8 -*-


import datetime
import serial
import serial.tools.list_ports


class GPS(object):

    _PORT = '/dev/ttyUSB0'
    _BAUD_RATE = 4800
    _DEVICE = 'USB-Serial Controller D'

    _SERIAL = None

    _SENTENCE_END = 'GPRMC'

    _TMP_SATELLITE = None

    connected = False

    rmc = dict(
        valid=None,
        timestamp=None,
        latitude=None,
        longitude=None,
        knots=None,
        course=None,
    )

    gsa = dict(
        mode=None,
        prns=None,
        pdop=None,
        hdop=None,
        vdop=None,
    )

    gsv = dict(

    )


    def __init__(self, port=False, baud_rate=False, device=False):

        self._PORT = self._PORT if not port else port
        self._BAUD_RATE = self._BAUD_RATE if not baud_rate else baud_rate
        self._DEVICE = self._DEVICE if not device else device

        self._SENTENCE_HANDLERS = {
            'GPRMC': self._PROCESS_RMC,
            'GPGSA': self._PROCESS_GSA,
            'GPGSV': self._PROCESS_GSV
        }


    def _connect(self):
        ports = list( serial.tools.list_ports.comports() )

        for p in ports:
            if p.description == self._DEVICE:
                self._PORT = p.device

        if not self._PORT:
            self.connected = False
            return False

        try:
            self._SERIAL = serial.Serial(self._PORT, self._BAUD_RATE)
            self.connected = True
            return True
        except:
            if not self._SERIAL == None:
                self._SERIAL.close()
            self.connected = False
            return False


    def run(self):
        while True:
            self.update()


    def update(self):

        if not self.connected:
            self._connect()

        while True:

            # read single line from serial
            line = self._read_line()

            # if line gives false reading exit while with return False
            if not line:
                return False

            # split line into tokens
            tokens = line.split(',')

            handler = self._SENTENCE_HANDLERS.get( tokens[0] )

            # test if handler exists
            if handler:
                handler(tokens[1:])

            # test if end sentence is found if true end while
            if tokens[0] == self._SENTENCE_END:
                break


    def _read_line(self):
        while True:
            try:
                line = self._SERIAL.readline().strip().decode("windows-1252")
            except:
                if not self._SERIAL == None:
                    self._SERIAL.close()
                self.connected = False
                return False

            if line.startswith('$'):
                return line[1:].split('*')[0]


    def _deg_to_dec(self, value):
        if not value:
            return None

        value = float(value)/100
        a = int(value)
        b = (value - a) * 100

        return a+(b/60)


    def _PROCESS_RMC(self, args):

        valid = args[1] == 'A'

        timestamp = datetime.datetime.strptime(args[8] + args[0],
            '%d%m%y%H%M%S.%f')

        latitude = self._deg_to_dec(args[2])
        longitude = self._deg_to_dec(args[4])

        #latitude = -latitude if args[3] in 'SW' else latitude
        #longitude = -longitude if args[3] in 'SW' else longitude

        knots = float(args[6]) if args[6] else None
        course = float(args[7]) if args[7] else None

        self.rmc = dict(
            valid=valid,
            timestamp=timestamp,
            latitude=latitude,
            longitude=longitude,
            knots=knots,
            course=course,
        )


    def _PROCESS_GSA(self, args):
        print(args)

        mode = int(args[1]) if args[1] else None
        prns = map(int, filter(None, args[2:14]))
        pdop = float(args[14]) if args[14] else None
        hdop = float(args[15]) if args[15] else None
        vdop = float(args[16]) if args[16] else None

        self.gsa = dict(
            mode=mode,
            prns=prns,
            pdop=pdop,
            hdop=hdop,
            vdop=vdop,
        )

    def _PROCESS_GSV(self, args):

        count = int(args[0])
        index = int(args[1])

        if index == 1:
            self._TMP_SATELLITE = {}

        for i in range(3, len(args), 4):
            data = args[i:i+4]

            if all(data):
                prn, elevation, azimuth, snr = data

                self._TMP_SATELLITE[ prn ] = [prn, elevation, azimuth, snr]

        if index == count:
            print(self._TMP_SATELLITE)
            self.gsv = dict(self._TMP_SATELLITE)

if __name__ == '__main__':

    GPS = GPS()
    GPS.update()
    print(GPS.rmc)
